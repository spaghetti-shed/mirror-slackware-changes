#!/bin/bash
#
# mirror-slackware-changes.sh
#
# Using the local slackware-current mirror, list all installed packages which
# have changed and those which are new.
#
# Copyright (C) 2020 by Iain Nicholson <iain.j.nicholson@gmail.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>
#
# 22nd June 2020 by Iain Nicholson.
#
# Change log:
# 2020-06-22 Initial creation.
#
MYNAME=`basename $0`

# Defaults
UPGRADEPKG=${UPGRADEPKG:="/sbin/upgradepkg"}
MIRROR=${MIRROR:-"/home/iain/downloads/mirrors/slackware64-current"}
PACKAGES=${MIRROR}/slackware64

DATESTAMP=`date "+%Y%m%d-%H%M%S"`
ALL_PKGS="package_list_all-${DATESTAMP}"
NEW_PKGS="package_list_new-${DATESTAMP}"
CHANGED_PKGS="package_list_changed-${DATESTAMP}"
UNCHANGED_PKGS="package_list_unchanged-${DATESTAMP}"
IGNORED_PKGS="package_list_ignored-${DATESTAMP}"
MULTILIB_PKGS="package_list_multilib-${DATESTAMP}"
TO_UPGRADE="package_list_to_upgrade-${DATESTAMP}"

usage()
{
    echo "${MYNAME}: Find all packages in a local slackware-current mirror which have changed compared with those currently installed."
    echo
    echo "Usage:"
    echo "  ${MYNAME}"
    echo
    echo "This script looks in a local Slackware current mirror, created using AlienBOB's mirror-slackware-current.sh script, and lists several things in the following files:"
    echo "  package_list_all-\${DATESTAMP}:        All packages found under the slackware64 dir in the local mirror."
    echo
    echo "  package_list_unchanged-\${DATESTAMP}:  All packages whose versions have not changed with respect to those currently installed."
    echo
    echo "  package_list_ignored-\${DATESTAMP}:    All packages which do not have a version currently installed."
    echo
    echo "  package_list_multilib-\${DATESTAMP}:   Packages which come from AlienBOB's Slackware multilib patches which should not be disturbed."
    echo
    echo "  package_list_changed-\${DATESTAMP}:    Packages which are installed and have changed, including those which may clash with AlienBOB's Slackware multilib patches."
    echo
    echo "  package_list_new-\${DATESTAMP}:        Packages which are installed and have changed, excluding those which may clash with AlienBOB's Slackware multilib patches, in other words, the list of packages safe to upgrade."
    echo
    echo "  package_list_to_upgrade-\${DATESTAMP}: A list of fully-specified paths to each package (in the local mirrir) to upgrade which can be used to feed to upgradepkg."
    echo
    echo
    echo "\${DATESTAMP} is a time and date stamp of the form YYYYMMDD-hhmmss appended to the file names to make comparing upgrade lists before and after refreshing the mirror easier."
    echo
    echo "The location of the local slackware-current mirror is specified in the environment variable MIRROR and the package subdirectory is specified in PACKAGES"
    echo
    echo "References"
    echo "  AlienBOB's mirror-slackware-current.sh:             http://www.slackware.com/~alien/tools/mirror-slackware-current.sh"
    echo "  AlienBOB's multilib patches for slackware-current:  http://www.slackware.com/~alien/multilib/current/"
    echo
}

if [ $# -ge 1 ]
then
    if [ "$1"  == "-h" ] || [ "$1" == "--help" ]
    then
        usage
        exit 0
    else
        echo "Run with -h or --help for usage information."
        exit 2
    fi
fi

if [ ! -d "${PACKAGES}" ]
then
    echo "Package directory ${PACKAGES} not found."
    exit 2
fi

for i in ${ALL_PKGS} ${NEW_PKGS} ${CHANGED_PKGS} ${IGNORED_PKGS} \
	 ${MULTILIB_PKGS} ${TO_UPGRADE}
do
    > ${i}
    if [ ! -f ${i} ]
    then
        echo "Failed to create ${i}"
        exit 2
    fi
done

${UPGRADEPKG} --dry-run `find ${PACKAGES} -name '*.t?z'` | sort > ${ALL_PKGS}
grep 'already installed' ${ALL_PKGS} > ${UNCHANGED_PKGS}
grep 'no installed package named' ${ALL_PKGS} > ${IGNORED_PKGS}
grep 'multillib' ${ALL_PKGS} > ${MULTILIB_PKGS}

cat ${ALL_PKGS}  | \
    grep -v 'already installed' | \
    grep -v 'no installed package named' \
    > ${CHANGED_PKGS}

grep -v 'multilib' ${CHANGED_PKGS} | sed 's/ would upgrade.*$//' > ${NEW_PKGS}

for i in `cat ${NEW_PKGS}`
do
    find "${PACKAGES}" -name "${i}*.t?z" >> ${TO_UPGRADE}
done

echo -n "Number of packages to be upgraded: "
wc -l ${TO_UPGRADE}

